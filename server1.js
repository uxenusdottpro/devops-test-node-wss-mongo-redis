require('dotenv').config();

var app = require('express')();
var cors = require('cors');
var http = require('http').createServer(app);
var io = require('socket.io')(process.env.WS_SERVER_PORT);

app.use(cors());

app.use(require('express').static(__dirname + '/static'));

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/static/index.html');
});

io.on('connection', (socket) => {
  console.log('a user connected');
});

http.listen(process.env.SERVER_PORT, () => {
  console.log(`listening on ${process.env.SERVER_HOST}:${process.env.SERVER_PORT}`);
});
